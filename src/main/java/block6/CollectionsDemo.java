package block6;


import java.util.*;

public class CollectionsDemo {
    static int numberStrokes;
     static int  maxEge=Integer.MIN_VALUE;
    static int findFirstSymbolMath(List<String> list, char c) {
        numberStrokes = 0;
        list.forEach((s -> {
            if (s.contains(String.valueOf(c))) numberStrokes++;
        }));
        return numberStrokes;
    }

    public static List<Human> getListWithoutPersonSurname(List<Human> humans, Human human) {
        List<Human> sameSurname = new java.util.ArrayList<>(Collections.emptyList());
        humans.forEach((s -> {
            if (!s.getSurname().equals(human.getSurname())) {
                sameSurname.add(s);
            }
        }));
        return sameSurname;
    }

    public static List<Human> getListWithoutSomePerson(List<Human> humans, Human human) {
        List<Human> sameSurname = new java.util.ArrayList<>(Collections.emptyList());
        humans.forEach((s -> {
            if (!s.equals(human)) {
                sameSurname.add(s);
            }
        }));
        return sameSurname;
    }


    public ArrayList<HashSet<Integer>> getSetsWithoutIntersectionWith(ArrayList<HashSet<Integer>> sets, HashSet<Integer> specSet) {
        ArrayList<HashSet<Integer>> result = new ArrayList<>(sets.size());

        for (HashSet<Integer> set : sets) {
            if (SetProcessor.intersection(set, specSet).size() == 0) {
                result.add(set);
            }
        }

        return result;
    }
      List<Human> getMaxEge(List<Human> humans){
        List<Human> maxEgeHuman = new java.util.ArrayList<>(Collections.emptyList());
        humans.forEach((human)->{
            if (human.getAge()>maxEge){
                maxEge=human.getAge();
            }
        });
        humans.forEach((human)->{
            if (human.getAge()==maxEge){
               maxEgeHuman.add(human);
            }
        });
       return maxEgeHuman;
    }

    public static ArrayList<Human> containId(Map<Integer, Human> map, Set<Integer> set){
        ArrayList<Human> humanMap = new ArrayList<>();
        for(Map.Entry<Integer, Human> e : map.entrySet()){
            if(!Collections.disjoint(Collections.singleton(e.getKey()), set)) humanMap.add(e.getValue());
        }

        return humanMap;
    }

    public static ArrayList<Integer> containId18(Map<Integer, Human> map) {
        ArrayList<Integer> humanMap = new ArrayList<>();
        for(Map.Entry<Integer, Human> e : map.entrySet()){
            if(e.getValue().getAge()>=18) humanMap.add(e.getKey());
        }
        return humanMap;
    }

    public static Map<Integer, Integer> containIdAge(Map<Integer, Human> map){
        Map<Integer, Integer> humanMap = new HashMap<>();
        for(Map.Entry<Integer, Human> e : map.entrySet()){
            humanMap.put(e.getKey(), e.getValue().getAge());
        }
        return humanMap;
    }


    public static Map<Integer, ArrayList<Human>> containIdAgeAndHuman(Map<Integer, Human> map, int age){
        Map<Integer, ArrayList<Human>> humanMap = new HashMap<Integer, ArrayList<Human>>();
        ArrayList<Human> list = new ArrayList<>();

        for(Map.Entry<Integer, Human> e : map.entrySet()){
            if(e.getValue().getAge()==age) {
                list.add(e.getValue());
            }
        }
        humanMap.put(age, list);

        return humanMap;
    }


}
