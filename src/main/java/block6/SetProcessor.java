package block6;


import java.util.HashSet;
import java.util.TreeSet;

public class SetProcessor {

    public static <T> HashSet<T> intersection(HashSet<T> set1, HashSet<T> set2) {
        HashSet<T> intersection = new HashSet<>(set1);
        intersection.retainAll(set2);
        return intersection;
    }

    public static <T> TreeSet<T> intersection(TreeSet<T> set1, TreeSet<T> set2) {
        TreeSet<T> intersection = new TreeSet<>(set1);
        intersection.retainAll(set2);
        return intersection;
    }

    public static <T> HashSet<T> union(HashSet<T> set1, HashSet<T> set2) {
        HashSet<T> intersection = new HashSet<>(set1);
        intersection.addAll(set2);
        return intersection;
    }

    public static <T> TreeSet<T> union(TreeSet<T> set1, TreeSet<T> set2) {
        TreeSet<T> intersection = new TreeSet<>(set1);
        intersection.addAll(set2);
        return intersection;
    }
}
