package block6;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class CollectionsDemoTest {
    @Test
    public void findFirstSymbolMath() {
        ArrayList<String> ar = new ArrayList<>();
        char d = 'B';
        ar.add("Bob");
        ar.add("Dog");
        ar.add("Bok");
        System.out.println(CollectionsDemo.findFirstSymbolMath(ar, d));
    }
    @Test
    public void getListWithoutPersonSurname() throws Exception {
        ArrayList<Human> people = new ArrayList<>();
        people.add(new Human("Black", "Bob", "Bobov", 12));
        people.add( new Human("Black", "Bob", "Bobovov", 12));
        people.add( new Human("White", "Bob", "Bobov", 12));
        people.add( new Human("White", "Bob", "Bobov", 12));
        Human h = new Human("White", "Bob", "Bobov", 12);

        ArrayList<Human> rez = new ArrayList<>();
        rez.add( new Human("Black", "Bob", "Bobov", 12));
        rez.add(new Human("Black", "Bob", "Bobovov", 12));
        assertEquals(CollectionsDemo.getListWithoutPersonSurname(people, h), rez);
    }

    @Test
    public void deleteByFamily() throws Exception {
        ArrayList<Human> people = new ArrayList<>();
        people.add(new Human("Black", "Bob", "Bobov", 12));
        people.add(new Human("Black", "Bob", "Bobov", 12));
        people.add(new Human("Black", "Bob", "Bobov", 12));
        people.add( new Human("White", "Bob", "Bobov", 12));
        Human h = new Human("White", "Bob", "Bobov", 12);

        ArrayList<Human> rez = new ArrayList<>();
        rez.add(new Human("Black", "Bob", "Bobov", 12));
        rez.add( new Human("Black", "Bob", "Bobov", 12));
        rez.add( new Human("Black", "Bob", "Bobov", 12));

        assertEquals(CollectionsDemo.getListWithoutSomePerson(people, h), rez);

    }


    @Test
    public void findMaxAge() throws Exception {
       List<Human> people = new ArrayList<>();
       CollectionsDemo collectionsDemo = new CollectionsDemo();
        people.add(new Human("Black", "Black", "Black", 15));
        people.add(new Human("Black", "Black", "Black", 17));
        people.add(new Human("Black", "Black", "Black", 14));
        people.add(new Student("Black", "Black", "Black", 17, "imit"));

        ArrayList<Human> check = new ArrayList<>();
        check.add(new Human("Black", "Black", "Black", 17));
        check.add(new Student("Black", "Black", "Black", 17, "imit"));
        Assert.assertEquals(collectionsDemo.getMaxEge(people),check);
    }

    @Test
    public void identificationInSet() throws Exception {
        Map<Integer, Human> map;
        map = new HashMap<Integer, Human>();
        map.put(1, new Human("Black", "Black", "Black", 15));
        map.put(2, new Human("Black", "Black", "Black", 15));
        map.put(3, new Human("Black", "Black", "Black", 15));
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(3);

        ArrayList<Human> rez = new ArrayList<>();
        rez.add(new Human("Black", "Black", "Black", 15));
        rez.add(new Human("Black", "Black", "Black", 15));

        Assert.assertEquals(CollectionsDemo.containId(map, set), rez);

    }

    @Test
    public void identificationTo18() throws Exception {
        Map<Integer, Human> map;
        map = new HashMap<Integer, Human>();
        map.put(1, new Human("Black", "Black", "Black", 25));
        map.put(2, new Human("Black", "Black", "Black", 17));
        map.put(3, new Human("Black", "Black", "Black", 147));
        map.put(4, new Human("Black", "Black", "Black", 27));

        ArrayList<Integer> rez = new ArrayList<>();
        rez.add(1);
        rez.add(3);
        rez.add(4);


        Assert.assertEquals(CollectionsDemo.containId18(map), rez);

    }

    @Test
    public void identificationByAge() throws Exception {
        Map<Integer, Human> map;
        map = new HashMap<Integer, Human>();
        map.put(1, new Human("Black", "Black", "Black", 25));
        map.put(2, new Human("Black", "Black", "Black", 17));
        map.put(3, new Human("Black", "Black", "Black", 135));
        map.put(4, new Human("Black", "Black", "Black", 27));

        Map<Integer, Integer> rez;
        rez = new HashMap<Integer, Integer>();
        rez.put(1,25);
        rez.put(2,17);
        rez.put(3,135);
        rez.put(4,27);


        Assert.assertEquals(CollectionsDemo.containIdAge(map), rez);

    }

    @Test
    public void ageAndHuman() throws Exception {
        Map<Integer, Human> map;
        map = new HashMap<Integer, Human>();
        map.put(1, new Human("u1", "Black", "Black", 25));
        map.put(2, new Human("u2", "Black", "Black", 17));
        map.put(3, new Human("u3", "Black", "Black", 135));
        map.put(4, new Human("u4", "Black", "Black", 25));
        int age = 25;


        Map<Integer, ArrayList<Human>> rez;
        rez = new HashMap<Integer, ArrayList<Human>>();
        ArrayList<Human> list = new ArrayList<>();
        list.add(new Human("u1", "Black", "Black", 25));
        list.add(new Human("u4", "Black", "Black", 25));
        rez.put(age,list);


        Assert.assertEquals(CollectionsDemo.containIdAgeAndHuman(map, age), rez);

    }
}
